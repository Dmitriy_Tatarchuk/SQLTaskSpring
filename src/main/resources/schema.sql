drop table if exists CLIENT;
CREATE TABLE CLIENT
(
	ID uuid                 GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	fullname                varchar(255)       
);
drop table if exists BILL;
CREATE TABLE BILL(
  	billNum			varchar(20) UNIQUE,
  	clientId 		UUID,
  	dateOfCreation          DATE
);
drop table if exists PAYMENT;
CREATE TABLE PAYMENT(
  	billNum			varchar(255),
        operationDate           DATE,
        operationSum            DOUBLE
);

SELECT CLIENT.clientId FROM CLIENT INNER JOIN BILL ON BILL.clientId = CLIENT.clientId; 

SELECT BILL.billNum FROM BILL INNER JOIN PAYMENT ON PAYMENT.billNum = BILL.billNum; 