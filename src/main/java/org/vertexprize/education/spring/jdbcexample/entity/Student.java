/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.education.spring.jdbcexample.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Класс Student при использовании jdbc
 * 
 * @author vaganovdv
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Student {
    
    private int id;             // Идентификатор записи студента 
    private String firstName;
    private String sureName;
    private String middleName;
    private String studentGroup;
    private int  year;
    
}
