/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.education.spring.jdbcexample.entity;

import java.sql.Date;
import java.util.UUID;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author dima
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Bill {

    private String billNum;
    private UUID clientId;
    private Date dateOfCreation;
    
}
