package org.vertexprize.education.spring.jdbcexample;

import jakarta.annotation.PostConstruct;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.vertexprize.education.spring.jdbcexample.entity.Client;
import org.vertexprize.education.spring.jdbcexample.entity.Student;
import org.vertexprize.education.spring.jdbcexample.service.DatabaseService;

@SpringBootApplication
public class JdbcExampleApplication {

    private static final Logger log = LoggerFactory.getLogger(JdbcExampleApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(JdbcExampleApplication.class, args);
    }

    @Autowired
    DatabaseService databaseService;

    @PostConstruct
    public void start() {
        /**
         * log.info("Подсчет количества студентов в базе данных ...");
         * databaseService.countStundens();
         *
         * log.info("Получение полного списка студентов в базе ...");
         * List<Student> stundents = databaseService.getAllStundents();
         *
         * if (!stundents.isEmpty()) { stundents.stream().forEach(stundent -> {
         * log.info(stundent.toString()); }); }
         *
         * log.info("Добавление студентов ... ");
         *
         *
         * Student student1 = new Student(); student1.setFirstName("Дмитрий");
         * student1.setMiddleName("Романович");
         * student1.setSureName("Татарчук"); student1.setStudentGroup("ПГС-21");
         * student1.setYear(5);
         *
         *
         *
         *
         * Student student2 = new Student(); student2.setFirstName("Иван");
         * student2.setMiddleName("Иванович"); student2.setSureName("Иванович");
         * student2.setStudentGroup("ПГС-21"); student2.setYear(5);
         *
         *
         * Student student3 = new Student(); student3.setFirstName("Кирилл");
         * student3.setMiddleName("Романович"); student3.setSureName("Кичигин");
         * student3.setStudentGroup("ПГС-21"); student3.setYear(5);
         *
         *
         * databaseService.saveStudent(student1);
         * databaseService.saveStudent(student2);
         * databaseService.saveStudent(student3);
         *
         * log.info("Получение полного списка студентов в базе ..."); stundents
         * = databaseService.getAllStundents();
         *
         * if (!stundents.isEmpty()) { stundents.stream().forEach(stundent -> {
         * log.info(stundent.toString()); }); }
         *
         * log.info("Удаление студента с идентификатором [2]");
         * databaseService.deleteStudentById(2L);
         *
         * log.info("Получение полного списка студентов в базе ..."); stundents
         * = databaseService.getAllStundents();
         *
         * if (!stundents.isEmpty()) { stundents.stream().forEach(stundent -> {
         * log.info(stundent.toString()); }); }
         *
         *
         * databaseService.deleteStudentById(100L);
         */

        log.info("Добавление клиентов");

        Client client1 = new Client();
        client1.setFullname("Клиент 1");

        Client client2 = new Client();
        client2.setFullname("Клиент 2");

        Client client3 = new Client();
        client3.setFullname("Клиент 3");

        Client client4 = new Client();
        client4.setFullname("Клиент 4");

        Client client5 = new Client();
        client5.setFullname("Клиент 5");

        Client client6 = new Client();
        client6.setFullname("Клиент 6");

        Client client7 = new Client();
        client7.setFullname("Клиент 7");

        Client client8 = new Client();
        client8.setFullname("Клиент 8");
        
        Client client9 = new Client();
        client9.setFullname("Клиент 9");
        
        Client client10 = new Client();
        client10.setFullname("Клиент 10");

        
        databaseService.saveClient(client1);
        databaseService.saveClient(client2);
        databaseService.saveClient(client3);
        databaseService.saveClient(client4);
        databaseService.saveClient(client5);
        databaseService.saveClient(client6);
        databaseService.saveClient(client7);
        databaseService.saveClient(client8);
        databaseService.saveClient(client9);
        databaseService.saveClient(client10);
    }

}
